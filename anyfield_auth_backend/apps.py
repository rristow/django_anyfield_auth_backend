from django.apps import AppConfig


class DjangoMailAuthBackendConfig(AppConfig):
    name = 'anyfield_auth_backend'
